#[macro_use]
extern crate rocket;

use std::process::Command;
use std::sync::atomic::{AtomicBool, Ordering};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::{Header, Status};
use rocket::response::status;
use rocket::{Request, Response, State};

struct CORS;

#[rocket::async_trait]
impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new("Access-Control-Allow-Methods", "POST, GET, PATCH, OPTIONS"));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}

#[get("/online")]
fn online(rebooting: &State<AtomicBool>) -> Result<&'static str, status::Custom<String>> {
    if rebooting.load(Ordering::Relaxed) {
        Ok("rebooting")
    } else {
        Ok("online")
    }
}

#[post("/reboot")]
fn reboot(rebooting: &State<AtomicBool>) -> Result<status::NoContent, status::Custom<String>> {
    Command::new("systemctl")
        .arg("reboot")
        .output()
        .map_err(|e| status::Custom(
            Status::InternalServerError,
            format!("Failed to execute reboot command: {}", e)
        ))?;

    rebooting.store(true, Ordering::Relaxed);
    Ok(status::NoContent)
}

#[post("/restart-kodi")]
fn restart_kodi() -> Result<status::NoContent, status::Custom<String>> {
    let output = Command::new("systemctl")
        .arg("restart")
        .arg("kodi-wayland.service")
        .output()
        .map_err(|e| status::Custom(
            Status::InternalServerError,
            format!("Failed to execute restart command for Kodi: {}", e)
        ))?;

    if output.status.success() {
        Ok(status::NoContent)
    } else {
        Err(status::Custom(
            Status::InternalServerError,
            format!("Failed to restart Kodi: {}", String::from_utf8_lossy(&output.stderr))
        ))
    }
}

#[post("/restart-pipewire")]
fn restart_pipewire() -> Result<status::NoContent, status::Custom<String>> {
    let output = Command::new("systemctl")
        .arg("-M")
        .arg("kodi@")
        .arg("--user")
        .arg("restart")
        .arg("pipewire")
        .arg("pipewire-pulse")
        .output()
        .map_err(|e| status::Custom(
            Status::InternalServerError,
            format!("Failed to execute restart command for PipeWire: {}", e)
        ))?;

    if output.status.success() {
        Ok(status::NoContent)
    } else {
        Err(status::Custom(
            Status::InternalServerError,
            format!("Failed to restart PipeWire: {}", String::from_utf8_lossy(&output.stderr))
        ))
    }
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(AtomicBool::from(false)) // rebooting
        .attach(CORS)
        .mount("/api", routes![
            restart_kodi, restart_pipewire, reboot, online
        ])
}