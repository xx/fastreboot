import {useState} from "react";
import {Button} from "@nextui-org/react";
import KodiIcon from "./assets/KodiIcon.tsx";
import {ErrorModal} from "./ErrorModal.tsx";
import {makePostRequest} from "./requests.ts";

export function RestartKodiButton() {
    const [isRestarting, setIsRestarting] = useState(false);
    const [error, setError] = useState<string | null>(null);

    const restart = async () => {
        setIsRestarting(true);
        setError(null);
        setError(await makePostRequest("https://panel.mordor.ee/api/restart-kodi"));
        setIsRestarting(false);
    }

    return (
        <>
            <Button
                startContent={<KodiIcon/>}
                className="w-52"
                color="primary"
                variant="shadow"
                isLoading={isRestarting}
                size="lg"
                onPress={restart}
            >
                Restart Kodi
            </Button>
            <ErrorModal error={error} onClose={() => setError(null)}/>
        </>
    );
}
