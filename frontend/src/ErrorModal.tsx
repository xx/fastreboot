import { Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader } from "@nextui-org/react";

interface ErrorModalProps {
    error: string | null;
    onClose: () => void;
}

export function ErrorModal({ error, onClose }: ErrorModalProps) {
    return (
        <Modal backdrop="blur" placement="auto" isOpen={error != null} onClose={onClose}>
            <ModalContent>
                <>
                    <ModalHeader>Error</ModalHeader>
                    <ModalBody>
                        <p>{error}</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" variant="light" onPress={onClose}>
                            Close
                        </Button>
                    </ModalFooter>
                </>
            </ModalContent>
        </Modal>
    );
}
