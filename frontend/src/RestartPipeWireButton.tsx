import {useState} from "react";
import {Button} from "@nextui-org/react";
import PipeWireIcon from "./assets/PipeWireIcon.tsx";
import {makePostRequest} from "./requests.ts";
import {ErrorModal} from "./ErrorModal.tsx";

export function RestartPipeWireButton() {
    const [isRestarting, setIsRestarting] = useState(false);
    const [error, setError] = useState<string | null>(null);

    const restart = async () => {
        setIsRestarting(true);
        setError(null);
        setError(await makePostRequest("https://panel.mordor.ee/api/restart-pipewire"));
        setIsRestarting(false);
    }

    return (
        <>
            <Button
                startContent={<PipeWireIcon/>}
                className="w-52"
                color="primary"
                variant="shadow"
                isLoading={isRestarting}
                size="lg"
                onPress={restart}
            >
                Restart PipeWire
            </Button>
            <ErrorModal error={error} onClose={() => setError(null)}/>
        </>
    );
}
