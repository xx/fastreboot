import {SVGProps} from "react"

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlSpace="preserve"
        width={20}
        height={20}
        viewBox="0 0 20 20"
        {...props}
    >
        <path
            id="path1"
            d="M16.534 3.593a1.638 1.638 0 1 0-2.248 2.387 6.137 6.137 0 0 1 1.943 4.512A6.237 6.237 0 0 1 10 16.722a6.237 6.237 0 0 1-6.23-6.23c0-1.73.69-3.332 1.944-4.512a1.639 1.639 0 1 0-2.248-2.387 9.509 9.509 0 0 0-2.974 6.899C.492 15.734 4.757 20 10 20c5.243 0 9.508-4.266 9.508-9.508a9.509 9.509 0 0 0-2.974-6.899z"
            style={{
                fill: "#ffffff",
            }}
        />
        <path
            id="path2"
            d="M10 10.492c.905 0 1.64-.587 1.64-1.312V1.311C11.64.587 10.904 0 10 0c-.905 0-1.64.587-1.64 1.311v7.87c0 .724.735 1.31 1.64 1.31z"
            style={{
                fill: "#ffffff",
            }}
        />
    </svg>
)
export default SvgComponent
