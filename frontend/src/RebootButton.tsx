import {useState} from "react";
import {Button, Popover, PopoverContent, PopoverTrigger} from "@nextui-org/react";
import axios from "axios";
import PowerIcon from "./assets/PowerIcon.tsx";
import {ErrorModal} from "./ErrorModal.tsx";
import {makePostRequest} from "./requests.ts";


export function RebootButton() {
    const [isRebooting, setIsRebooting] = useState(false);
    const [isPopoverOpen, setIsPopoverOpen] = useState(false);
    const [error, setError] = useState<string | null>(null);

    const reboot = async () => {
        setIsPopoverOpen(false);
        setIsRebooting(true);
        setError(null); // Clear any previous errors

        const errorMessage = await makePostRequest("https://panel.mordor.ee/api/reboot");
        if (errorMessage) {
            setError(errorMessage);
            setIsRebooting(false);
            return;
        }

        let isOnline = false;
        while (!isOnline) {
            // sleep for 5 seconds
            await new Promise(r => setTimeout(r, 5000));
            try {
                const res = await axios.get("https://panel.mordor.ee/api/online", {timeout: 2000});
                if (res.status === 200 && res.data === "online") {
                    isOnline = true;
                }
            } catch (err) {
                // this is expected when rebooting
                console.error(err);
            }
        }

        setIsRebooting(false);
    };

    return (
        <>
            <Popover placement="top" backdrop="blur" isOpen={isPopoverOpen}
                     onOpenChange={(open) => setIsPopoverOpen(open)}>
                <PopoverTrigger>
                    <Button startContent={<PowerIcon/>} className="w-52" color="danger" isLoading={isRebooting}
                            variant="shadow" size="lg">
                        Reboot
                    </Button>
                </PopoverTrigger>
                <PopoverContent>
                    <div className="flex flex-col gap-3 items-center p-3">
                        <p>Really reboot?</p>
                        <Button color="danger" onPress={reboot} variant="shadow" className="w-0">Yes</Button>
                    </div>
                </PopoverContent>
            </Popover>
            <ErrorModal error={error} onClose={() => setError(null)}/>
        </>
    );
}
