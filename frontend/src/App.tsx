import {NextUIProvider} from "@nextui-org/react";
import {RestartKodiButton} from "./RestartKodiButton.tsx";
import {RestartPipeWireButton} from "./RestartPipeWireButton.tsx";
import {RebootButton} from "./RebootButton.tsx";

function App() {
    return <NextUIProvider>
        <div className="flex flex-col gap-3 justify-center items-center h-screen md:flex-row">
                <RestartKodiButton/>
                <RestartPipeWireButton/>
                <RebootButton/>
        </div>
    </NextUIProvider>
}

export default App
