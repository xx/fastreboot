import axios from "axios";

export const makePostRequest = async (url: string): Promise<string | null> => {
    try {
        const response = await axios.post(url);
        if (!(response.status === 200 || response.status === 204)) {
            return response.data;
        }
        return null;
    } catch (err: unknown) {
        console.error(err);
        if (axios.isAxiosError(err)) {
            if (err.response) {
                return err.response.data;
            } else if (err.request) {
                return "No response from server. Please try again.";
            } else {
                return err.message;
            }
        } else {
            return "Unknown error."
        }
    }
};
